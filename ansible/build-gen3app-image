#!/bin/bash
# Script to build new image for the app VMs

set -o nounset
set -o errexit
set -o pipefail

#set -x # activate debugging from here

# Backup current image
LOCATION=/shared/wur/sali/images/gen3app
BASE_ARCHIVE=/shared/wur/sali/images/archive/$(basename "${LOCATION}")
PLAYBOOK=build-gen3app.yml
VERSION="${LOCATION}"/etc/sali_version
run_full=true

# Help
usage() {
    echo "You'll need to decide whether to archive/redownload everything, or overwrite the existing image:" >&2
    echo "Usage: $0 [--full|--fast]" >&2
    echo "  --full  Archive current image & redownload everything"
    echo "  --fast  Overwrite the existing image"
    exit 1
}

# Check whether we got any options
if [[ $# -ne 1 ]]; then
    usage
fi

# Parse options using getopt, and break if they are wrong
if ! OPTIONS=$(getopt --options '' --long full,fast --name "$0" -- "$@")
then
    usage
fi

eval set -- "$OPTIONS"

# Get full or fast option
while true; do
    case "$1" in
        --full) run_full=true  ; shift ;;
        --fast) run_full=false ; shift ;;
        # -- means the end of the arguments; drop this, and break out of the while loop
        --) shift; break ;;
        *)  echo "Error: Unknown option: $1" >&2
            usage
            ;;
    esac
done

# Backup current image if --full option is provided
if $run_full; then
    if [[ -s "$VERSION" ]]; then
        DATE=$(date --date=@"$(cat "$VERSION")" -Iminutes)
    else
        DATE=$(date -Iminutes)
    fi

    if [[ -e "$LOCATION" ]]; then
        # Use multithreaded xz, only using half of the available cores
        echo "Creating ${BASE_ARCHIVE}.${DATE}.tar.xz"
        tar --exclude="${LOCATION}"/proc --exclude="${LOCATION}"/sys --use-compress-program="xz --threads=$(( $(nproc) / 2 ))" -cf "${BASE_ARCHIVE}"."${DATE}".tar.xz "${LOCATION}" \
        && echo "${BASE_ARCHIVE}.${DATE}.tar.xz created :" \
        && ls -lh "${BASE_ARCHIVE}"."${DATE}".tar.xz \
        && mv -v "$LOCATION" "$BASE_ARCHIVE"."${DATE}"
    fi
fi

# Create new image
if $run_full; then
    echo "Creating archive/backup, building full new image from ${PLAYBOOK}"
else
    echo "Skipping archive/backup, rebuilding image quickly from ${PLAYBOOK}"
fi
export ANSIBLE_NOCOWS=1
ansible-playbook --connection=local -i127.0.0.1, -l127.0.0.1 "${PLAYBOOK}"
