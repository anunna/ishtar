#Copyright (c) 2023 Wageningen University
#Author: Gwen Dawes <gwen.dawes@wur.nl>
#        Jan van Haarst <jan.vanhaarst@wur.nl>
#        Fons Prinsen <fons.prinsen@wur.nl>

- set_fact:
    node_type: app

- fail: msg="You must define chroot_target!"
  when: chroot_target is undefined

- name: install prerequisites
  apt:
    pkg:
      - debootstrap

- name: Check chroot presence
  stat:
    path: "{{chroot_target}}"
  register: chroot_stat

- name: debootstrap initial image
  command: "debootstrap focal {{chroot_target}}"
  when: chroot_stat.stat.isdir is not defined

- name: Add timestamp for version tracking
  command: "chroot {{chroot_target}} /bin/bash -c 'date +%s > /etc/sali_version'"

- name: Add base repos
  copy:
    dest: "{{chroot_target}}/etc/apt/sources.list"
    content: |
      deb http://nl.archive.ubuntu.com/ubuntu focal main restricted universe multiverse
      deb http://nl.archive.ubuntu.com/ubuntu focal-updates main restricted universe multiverse
      deb http://nl.archive.ubuntu.com/ubuntu focal-security main restricted universe multiverse

- name: Update base software
  command: "chroot {{chroot_target}} /bin/bash -c 'apt-get update && apt-get dist-upgrade --assume-yes'"

- name: Install extra base software
  command: "chroot {{chroot_target}} /bin/bash -c 'apt-get update && apt-get install -y nano netbase isc-dhcp-client systemd-sysv sudo initramfs-tools cloud-init netplan.io overlayroot rsync openssh-server ipmitool nfs-common libipmimonitoring6 freeipmi-tools jq curl libcurl4-openssl-dev'"

- name: Add a root password
  command: "chroot {{chroot_target}} /bin/bash -c 'echo 'root:{{node_root_hash}}' | chpasswd -e'"

- name: Make client ssh folder
  file:
    path: "{{chroot_target}}/root/.ssh"
    state: directory
    owner: root
    group: root
    mode: '0700'

- name: Make client ssh authorized_keys file
  file:
    path: "{{chroot_target}}/root/.ssh/authorized_keys"
    state: touch
    owner: root
    group: root
    mode: '0600'

- name: Copy ssh key to client
  copy:
      dest: "{{chroot_target}}/root/.ssh/authorized_keys"
      mode: '0600'
      content: "{{root_ssh_pubkey}}"

- block:
  - name: "Set ssh host private keys"
    copy:
      dest: "{{chroot_target}}/etc/ssh/ssh_host_{{item.type}}_key"
      mode: '0600'
      content: "{{item.key}}"
    with_items: "{{client_ssh_keys}}"

  - name: "Generate ssh host public keys"
    command: "chroot {{chroot_target}} /bin/bash -c 'ssh-keygen -y -f /etc/ssh/ssh_host_{{item.type}}_key > /etc/ssh/ssh_host_{{item.type}}_key.pub'"
    with_items: "{{client_ssh_keys}}"
  when: login_ssh_keys is defined

#sshd_config
- name: Set sshd_config
  template:
    src: "templates/app_sshd_config.j2"
    dest: "{{chroot_target}}/etc/ssh/sshd_config"
    owner: root
    group: root
    mode: "0644"

#efi config unnecessary here as it's set in the post_installer scripts in SALI
#Hostname gets updated by sali
- name: Set hostname
  copy:
    dest: "{{chroot_target}}/etc/hostname"
    content: gen3app

#Disable ipv6 in sysctl.d/ipv6.conf
- name: Disable ipv6 in sysctl.d/200-ipv6.conf
  lineinfile:
    path: "{{chroot_target}}/etc/sysctl.d/200-ipv6.conf"
    line: "{{ item.line }}"
    create: yes
  loop:
    - { line: 'net.ipv6.conf.all.disable_ipv6=1' }
    - { line: 'net.ipv6.conf.default.disable_ipv6=1' }

#Netplan config created by sali
#fstab created by sali

- name: Remove machine-id from /etc
  file: 
    path: "{{chroot_target}}/etc/machine-id"
    state: absent

- name: Remove machine-id from /var/lib/dbus
  file: 
    path: "{{chroot_target}}/var/lib/dbus/machine-id"
    state: absent

- name: Check resolv.conf state
  stat:
    path: "{{chroot_target}}/etc/resolv.conf"
  register: resolvconf_stat

- name: Remove resolv.conf symlink
  file:
    path: "{{chroot_target}}/etc/resolv.conf"
    state: absent
  when: resolvconf_stat.stat.islnk is defined and resolvconf_stat.stat.islnk

- name: Add resolv.conf contents
  copy:
    dest: "{{chroot_target}}/etc/resolv.conf"
    content: |
      nameserver {{shared_master['networks'][default_network]['ip'] | ipaddr('address')}}
      search {{networks[default_network]['zone']}}

- name: Install lustre build packages
  command: "chroot {{chroot_target}} /bin/bash -c 'apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -y install pkg-config perl lsof autoconf automake dpatch debhelper libltdl-dev bison libgfortran4 pciutils ethtool make swig gcc chrpath graphviz autotools-dev quilt flex dkms tk tcl libnuma1 gfortran libyaml-dev libnl-3-200 libnl-route-3-200 zlib1g-dev module-assistant libreadline-dev libselinux-dev libsnmp-dev libssl-dev gcc-8'"

- name: Check kernel module presence
  stat:
    path: "{{chroot_target}}/lib/modules/{{lustre_client_kernel_version}}-generic/"
  register: kernel_module_stat

- name: Get current lustre source package name
  shell: "curl --connect-timeout 5 --max-time 10 --retry 5 --retry-delay 0 --retry-max-time 40 https://downloads.whamcloud.com/public/lustre/lustre-{{lustre_client_version}}/ubuntu2204/client/Packages | grep Filename | grep lustre-source | cut -d '/' -f 2"
  args: {warn: false}
  register: lustre_source_shell

- set_fact:
    lustre_source_package: "{{lustre_source_shell.stdout}}"

- name: Lustre kernel installation
  block:
  - name: Get current master kernel
    command: "uname -r"
    register: uname_r_shell

  - name: Get current kernel package version
    shell: "curl --connect-timeout 5 --max-time 10 --retry 5 --retry-delay 0 --retry-max-time 40 {{lustre_client_kernel_pool}} | grep {{lustre_client_kernel_version}}-generic | grep amd | sed 's/<[^>]*>/ /g' | sed 's/  */ /g' | cut -d ' ' -f 2 | grep linux-headers | cut -d '-' -f 3-8"
    args: {warn: false}
    register: kernel_generic_version_shell

  - name: Get current kernel package version (all)
    shell: "curl --connect-timeout 5 --max-time 10 --retry 5 --retry-delay 0 --retry-max-time 40 {{lustre_client_kernel_pool}} | grep {{lustre_client_kernel_version}} | grep all | sed 's/<[^>]*>/ /g' | sed 's/  */ /g' | cut -d ' ' -f 2 | grep headers"
    args: {warn: false}
    register: linux_headers_all_shell

  - set_fact:
      master_kernel: "{{uname_r_shell.stdout}}"
      kernel_version_deb_suffix: "{{kernel_generic_version_shell.stdout}}"
      linux_headers_all_package: "{{linux_headers_all_shell.stdout}}"

  - name: Add kernel modules workaround directory
    file:
      path: "{{chroot_target}}/lib/modules/{{lustre_client_kernel_version}}-generic"
      state: directory

  - name: Remove existing kernel modules directory
    file:
      path: "{{chroot_target}}/lib/modules/{{master_kernel}}"
      state: absent

  - name: Add kernel modules workaround symlink
    file:
      path: "{{chroot_target}}/lib/modules/{{master_kernel}}"
      src: "{{lustre_client_kernel_version}}-generic"
      state: link
      force: yes

  - name: Install kernel dependency packages
    command: "chroot {{chroot_target}} /bin/bash -c 'apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -y install crda wireless-crda'"
  - name: Remove current headers
    command: "chroot {{chroot_target}} /bin/bash -c 'apt-get -y remove linux-headers*'"

  - name: Download linux headers (specific)
    command: "wget -q {{lustre_client_kernel_pool}}/linux-headers-{{kernel_version_deb_suffix}} -O {{chroot_target}}/opt/linux-headers-{{kernel_version_deb_suffix}}"
    args: {warn: false}
  - name: Download linux image (unsigned)
    command: "wget -q {{lustre_client_kernel_pool}}/linux-image-unsigned-{{kernel_version_deb_suffix}} -O {{chroot_target}}/opt/linux-image-unsigned-{{kernel_version_deb_suffix}}"
    args: {warn: false}
  - name: Download linux modules
    command: "wget -q {{lustre_client_kernel_pool}}/linux-modules-{{kernel_version_deb_suffix}} -O {{chroot_target}}/opt/linux-modules-{{kernel_version_deb_suffix}}"
    args: {warn: false}
  - name: Download linux extra modules
    command: "wget -q {{lustre_client_kernel_pool}}/linux-modules-extra-{{kernel_version_deb_suffix}} -O {{chroot_target}}/opt/linux-modules-extra-{{kernel_version_deb_suffix}}"
    args: {warn: false}
  - name: Download linux headers (generic)
    command: "wget -q {{lustre_client_kernel_pool}}/{{linux_headers_all_package}} -O {{chroot_target}}/opt/{{linux_headers_all_package}}"
    args: {warn: false}
  - name: Install all kernel deb files
    command: "chroot {{chroot_target}} /bin/bash -c 'dpkg -i /opt/*.deb'"
  when: kernel_module_stat.stat.isdir is not defined

- name: Install grub-efi
  command: "chroot {{chroot_target}} /bin/bash -c 'apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -y install grub-efi grub-efi-amd64'"

- name: Install final lustre package dependencies
# dpkg fails on linux-headers-generic, so we install it. But it's the wrong version, so we remove it again after building lustre (it's a hack, sorry)
  command: "chroot {{chroot_target}} /bin/bash -c 'apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -y install attr libpython3-stdlib libpython3.8-minimal libpython3.8-stdlib python3-dev python-is-python3 libsgutils2-2 libtool-bin mpi-default-bin sg3-utils mpi-default-dev ed libkeyutils1 libkeyutils-dev libaio1 libmount-dev libnl-genl-3-dev git libkrb5-dev linux-headers-generic quota selinux-utils'"

- name: Check lustre kernel module presence
  stat:
    path: "{{chroot_target}}/lib/modules/{{lustre_client_kernel_version}}-generic/updates/net/ko2iblnd.ko"
  register: lustre_module_stat

- name: Lustre installation from source
  block:
  - name: Download lustre source
    command: "wget -q https://downloads.whamcloud.com/public/lustre/lustre-{{lustre_client_version}}/ubuntu2204/client/{{lustre_source_package}} -O {{chroot_target}}/opt/lustre-source.deb"
    args: {warn: false}
  - name: Unpack downloaded deb
    command: "chroot {{chroot_target}} /bin/bash -c 'dpkg-deb -R /opt/lustre-source.deb /opt/lustre-source-deb'"
  - name: Untar source package
    command: "chroot {{chroot_target}} /bin/bash -c 'tar -xf /opt/lustre-source-deb/usr/src/lustre*.tar.bz2 -C /opt'"
  - name: Generate configure scripts (using autoconf)
    command: "chroot {{chroot_target}} /bin/bash -c 'cd /opt/modules/lustre/ && sh autogen.sh'"
  - name: Configure Lustre packages
    command: "chroot {{chroot_target}} /bin/bash -c 'cd /opt/modules/lustre/ && CC=gcc-8 ./configure --with-o2ib --disable-server --with-linux=/lib/modules/{{lustre_client_kernel_version}}-generic/build --with-kernel-source-header=/usr/src/linux-headers-{{lustre_client_kernel_version}}-generic/'"
  - name: Make Lustre packages
    command: "chroot {{chroot_target}} /bin/bash -c 'cd /opt/modules/lustre/ && CC=gcc-8 make debs -j $(( $(nproc) / 2))'"
  - name: Install packages
    command: "chroot {{chroot_target}} /bin/bash -c 'dpkg -i /opt/modules/lustre/debs/lustre*.deb'"
  - name: Remove lustre-source (to allow rebuilding packages without failures)
    command: "chroot {{chroot_target}} /bin/bash -c 'apt-get -y remove lustre-source'"
  - name: Remove kernel modules workaround symlink
    file:
      path: "{{chroot_target}}/lib/modules/{{master_kernel}}"
      state: absent
  when: lustre_module_stat.stat.isreg is not defined

- name: Remove non-5.15 linux-headers-generic
  command: "chroot {{chroot_target}} /bin/bash -c 'apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -y remove linux-headers-generic'"

#lnet.conf handled by sali

- name: Make lnet start on boot
  file:
    path: "{{chroot_target}}/etc/systemd/system/multi-user.target.wants/lnet.service"
    src: "/lib/systemd/system/lnet.service"
    state: link
    force: yes

# This is required after the hwe kernel with lustre 2.15.5. It's a hack, we shouldn't need to run this 'manually' like this. Figure out how this actually works and make it pretty
- name: Run depmod before modprobing lnet kernel module
  lineinfile:
    path: "{{chroot_target}}/lib/systemd/system/lnet.service"
    insertafter: '^RemainAfterExit'
    line: ExecStartPre=/usr/sbin/depmod

#User auth configs
- name: Install auth config tools (1)
  command: "chroot {{chroot_target}} /bin/bash -c 'apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -y install sssd sssd-tools samba-common'"
- name: Install auth config tools (2)
  command: "chroot {{chroot_target}} /bin/bash -c 'apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -y install nslcd libnss-ldapd'"

# sssd
- name: Set up sssd config
  template:
    src: templates/client_sssd.conf.j2
    dest: "{{chroot_target}}/etc/sssd/sssd.conf"
    owner: root
    group: root
    mode: '0600'

- name: Set up nslcd config
  template:
    src: templates/client_nslcd.conf.j2
    dest: "{{chroot_target}}/etc/nslcd.conf"
    owner: root
    group: nslcd
    mode: '0640'
- name: Make ldap ssl cert folder
  file:
    path: "{{chroot_target}}/etc/ldap/ssl/"
    state: directory

- name: "Place ldap certificates"
  copy:
    dest: "{{chroot_target}}/etc/ldap/ssl/{{item.name}}"
    mode: '0600'
    content: "{{item.cert}}"
  with_items: "{{ldap_certs}}"

- name: Set up nsswitch
  template:
    src: templates/nsswitch.conf.j2
    dest: "{{chroot_target}}/etc/nsswitch.conf"
    owner: root
    group: root
    mode: '0644'

#Remove sssd sockets from systemd target
- name: Remove boot dependencies on sssd sockets
  file:
    path: "{{chroot_target}}/etc/systemd/system/sssd.service.wants/{{ item.path }}"
    state: absent
  loop:
    - { path: 'sssd-autofs.socket' }
    - { path: 'sssd-nss.socket' }
    - { path: 'sssd-pam-priv.socket' }
    - { path: 'sssd-pam.socket' }
    - { path: 'sssd-ssh.socket' }
    - { path: 'sssd-sudo.socket' }
    - { path: 'sssd-pac.socket' }

# Disable nscd service
- name: Remove nscd startup (stops sssd complaining)
  command: "find {{chroot_target}}/etc/ -name 'S01nscd' -exec rm {} \\;"
- name: Remove nscd startup from systemd too
  file: 
    path: "{{chroot_target}}/etc/systemd/system/multi-user.target.wants/nscd.service"
    state: absent
- name: mask nscd.service
  file:
    path: "{{chroot_target}}/etc/systemd/system/nscd.service"
    src: "/dev/null"
    state: link
    force: yes
# /Disable nscd service

# Set up SMB just like a compute node
- name: Set up smb.conf
  template:
    src: templates/smb.conf.j2
    dest: "{{chroot_target}}/etc/samba/smb.conf"
    owner: root
    group: root
    mode: '0644'

#Copy filesystem mount files in
- name: Set up mount configs
  template:
    src: templates/client_mount.j2
    dest: "{{chroot_target}}/etc/systemd/system{{item.path}}.mount"
    owner: root
    group: root
    mode: '0644'
  with_items: "{{client_mounts}}"

- name: Set up automount configs
  template:
    src: templates/client_automount.j2
    dest: "{{chroot_target}}/etc/systemd/system{{item.path}}.automount"
    owner: root
    group: root
    mode: '0644'
  with_items: "{{client_mounts}}"

- name: Make mounts automount on boot
  file:
    path: "{{chroot_target}}/etc/systemd/system/multi-user.target.wants{{item.path}}.automount"
    src: "/etc/systemd/system{{item.path}}.automount"
    state: link
    force: yes
  with_items: "{{client_mounts}}"

- name: Create /etc/idmapd.conf
  template:
      src: templates/login_idmapd.conf.j2
      dest: "{{chroot_target}}/etc/idmapd.conf"
      owner: root
      group: root
      mode: '0644'

- name: Include slurmd task list in play
  include_tasks:
    file: clients/slurmc.yaml

#Overlayroot, disabled for dev phase, might want it later
#- name: Set overlayroot on
#  lineinfile:
#    path: "{{chroot_target}}/etc/overlayroot.conf"
#    regexp: '^overlayroot=.*'
#    line: overlayroot="tmpfs:swap=1,recurse=0"

- name: Disable quiet grub
  lineinfile:
    path: "{{chroot_target}}/etc/default/grub"
    regexp: '.*GRUB_CMDLINE_LINUX_DEFAULT=.*'
    line: '#GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"'

- name: Set grub boot options for swap accounting and cgroupsv2
  lineinfile:
    path: "{{chroot_target}}/etc/default/grub"
    regexp: '.*GRUB_CMDLINE_LINUX=.*'
    line: 'GRUB_CMDLINE_LINUX="swapaccount=1 systemd.unified_cgroup_hierarchy=1"'

- name: Enable log forwarding to master
  lineinfile:
    path: "{{chroot_target}}/etc/rsyslog.conf"
    line: "*.* @{{shared_master['networks'][default_network]['fqdn']}}:514"

#Postfix
- name: Install system postfix
  command: "chroot {{chroot_target}} /bin/bash -c 'apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -y install postfix mailutils postfix-pcre'"

- name: Set up main postfix config
  template:
    src: "templates/postfix_main.cf.j2"
    dest: "{{chroot_target}}/etc/postfix/main.cf"
    owner: root
    group: root
    mode: "0644"

- name: Set up generic mapping config
  template:
    src: "templates/postfix_generic.pcre.j2"
    dest: "{{chroot_target}}/etc/postfix/generic.pcre"
    mode: "0644"

- name: Set up sender_canonical_maps
  template:
    src: "templates/postfix_sender_canonical_maps.j2"
    dest: "{{chroot_target}}/etc/postfix/sender_canonical_maps"
    mode: "0644"

- name: Set up header_checks
  template:
    src: "templates/postfix_header_checks.j2"
    dest: "{{chroot_target}}/etc/postfix/header_checks"
    mode: "0644"

- name: Set up smtp_header_checks
  template:
    src: "templates/postfix_smtp_header_checks.j2"
    dest: "{{chroot_target}}/etc/postfix/smtp_header_checks"
    mode: "0644"

- name: Set up sasl_passwd
  template:
    src: "templates/postfix_sasl_passwd.j2"
    dest: "{{chroot_target}}/etc/postfix/sasl/sasl_passwd"
    mode: "0600"

- name: Configure remapping database
  command: "postmap {{chroot_target}}/etc/postfix/generic.pcre"

- name: postmap header_checks
  command: "postmap {{chroot_target}}/etc/postfix/header_checks"

- name: postmap smtp_header_checks
  command: "postmap {{chroot_target}}/etc/postfix/smtp_header_checks"

- name: postmap sender_canonical_maps
  command: "postmap {{chroot_target}}/etc/postfix/sender_canonical_maps"

- name: postmap sasl_password
  command: "postmap {{chroot_target}}/etc/postfix/sasl/sasl_passwd"

- name: Set up mailutils config
  template:
    src: "templates/mailutils.conf.j2"
    dest: "{{chroot_target}}/etc/mailutils.conf"
    mode: "0644"

- name: Disable local logging of mail
  lineinfile:
    path: "{{chroot_target}}//etc/rsyslog.d/50-default.conf"
    regexp: ".*mail."
    state: absent

#Telegraf
- name: Add influx repo
  copy:
   dest: "{{chroot_target}}/etc/apt/sources.list.d/influxdata.list"
   content: |
     deb https://repos.influxdata.com/debian stable main

- name: Add influx key
  command: "chroot {{chroot_target}} /bin/bash -c 'wget -O- -q https://repos.influxdata.com/influxdata-archive_compat.key | apt-key add -'"
- name: Install telegraf and dependencies
  command: "chroot {{chroot_target}} /bin/bash -c 'apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -y install telegraf lm-sensors smartmontools nvme-cli'"

- name: Set up telegraf config
  template:
   src: "templates/client_telegraf.conf.j2"
   dest: "{{chroot_target}}/etc/telegraf/telegraf.conf"

- name: Set up telegraf sudoers file
  copy:
   dest: "{{chroot_target}}/etc/sudoers.d/telegraf"
   content: |
     Defaults:telegraf !requiretty, !syslog
     telegraf        ALL     =       (root)  NOPASSWD:       /usr/sbin/smartctl
     telegraf        ALL     =       (root)  NOPASSWD:       /usr/sbin/nvme
     telegraf        ALL     =       (root)  NOPASSWD:       /bin/cat /proc/slabinfo

- name: Stop sudo complaining about telegraf
  lineinfile:
    path: "{{chroot_target}}/etc/nsswitch.conf"
    regexp: "^(sudoers:.*files)"
    line: '\1'
    backrefs: yes

- name: Set up telegraf service
  template:
   src: templates/telegraf.service.j2
   dest: "{{chroot_target}}/etc/systemd/system/telegraf.service"
   mode: '0644'

- name: Make telegraf start on boot
  file:
   path: "{{chroot_target}}/etc/systemd/system/multi-user.target.wants/telegraf.service"
   src: "/etc/systemd/system/telegraf.service"
   state: link
   force: yes

- name: Create telegraf pid file folder
  file:
   path: "{{chroot_target}}/var/run/telegraf"
   state: directory
   mode: '0777'
   force: yes 

- name: Create telegraf pid file
  file:
   path: "{{chroot_target}}/var/run/telegraf/telegraf.pid"
   state: touch
   mode: '0777'
   force: yes

#Lmod
- name: Install lmod dependencies
  command: "chroot {{chroot_target}} /bin/bash -c 'apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -y install liblua5.3-0 liblua5.3-dev lua5.3 lua-posix luarocks tcl tcl-dev'"

- name: HACK - symlink the LMOD path to match SL7
  file:
    src: "{{apps_path}}/lmod"
    dest: "{{chroot_target}}/usr/share/lmod"
    force: yes
    state: link

- name: Add Lmod line to bashrc
  blockinfile:
    path: "{{chroot_target}}/etc/bash.bashrc"
    block: |
      #Set a default manpath
      export MANPATH=/usr/share/man
      # Enable lmod
      . {{apps_path}}/lmod/current/init/bash
      #HACK - this allows for backwards compatibility underlay with SL7
      export LMOD_CMD=/usr/share/lmod/lmod/libexec/lmod
      #Set the modulepath
      export MODULEPATH={{modules_path}}

- name: Remove Ubuntu motd
  replace:
      path: "{{chroot_target}}/etc/pam.d/{{item}}"
      regexp: '^(session.*optional.*motd=/run/motd.dynamic)'
      replace: '#\1'
  with_items:
  - sshd
  - login

# We won't need shorewall, but it might be a good idea to set up some firewalling, since we're hosting services on internalnet
#Shorewall
#- name: Install shorewall
#  command: "chroot {{chroot_target}} /bin/bash -c 'apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -y install shorewall'"
#
#- name: Set up shorewall config
#  template:
#    src: "templates/shorewall.conf.j2"
#    dest: "{{chroot_target}}/etc/shorewall/shorewall.conf"
#    owner: root
#    group: root
#    mode: "0640"
#
#- name: Set up common shorewall sub config files
#  template:
#    src: "templates/shorewall_{{item}}.j2"
#    dest: "{{chroot_target}}/etc/shorewall/{{item}}"
#    owner: root
#    group: root
#    mode: "0640"
#  with_items:
#  - start
#  - params
#  - conntrack
#  - policy
#  - zones
#
#- name: Set up specialised shorewall interfaces file
#  template:
#    src: "templates/login_shorewall_interfaces.j2"
#    dest: "{{chroot_target}}/etc/shorewall/interfaces"
#    owner: root
#    group: root
#    mode: "0640"
#  with_items:
#  - node_type: "{{node_type}}"
#
#- name: Set up specialised shorewall rules file
#  template:
#    src: "templates/login_shorewall_rules.j2"
#    dest: "{{chroot_target}}/etc/shorewall/rules"
#    owner: root
#    group: root
#    mode: "0640"
#  with_items:
#  - node_type: "{{node_type}}"

#Fail2ban
#Don't want fail2ban like this, but we need to think about security a bit
#- name: Install fail2ban
#  command: "chroot {{chroot_target}} /bin/bash -c 'apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -y install fail2ban'"
#
#- name: Set up fail2ban jail config
#  template:
#    src: "templates/fail2ban.jail.local.j2"
#    dest: "{{chroot_target}}/etc/fail2ban/jail.local"
#    owner: root
#    group: root
#    mode: "0640"
#
#- name: Set up fail2ban filter configs
#  template:
#    src: "templates/fail2ban.filter.{{item}}.j2"
#    dest: "{{chroot_target}}/etc/fail2ban/filter.d/{{item}}"
#    owner: root
#    group: root
#    mode: "0640"
#  with_items:
#  - sshd_invalid.conf
#  - wur_sshd.conf
#  - sshd-whitelist.conf
#
#- name: Set up fail2ban action configs
#  template:
#    src: "templates/fail2ban.action.{{item}}.j2"
#    dest: "{{chroot_target}}/etc/fail2ban/action.d/{{item}}"
#    owner: root
#    group: root
#    mode: "0640"
#  with_items:
#  - ignoreip.conf

#ntp
- name: Include ntp task list in play
  include_tasks:
    file: clients/ntp.yaml

- name: Setup umask for mkhomedir
  lineinfile:
    path: "{{chroot_target}}/usr/share/pam-configs/mkhomedir"
    regexp: "^(.*pam_mkhomedir.so)$"
    line: '\1 umask=0077'
    backrefs: yes

#Do we want this? We'll be mounting homeshared..?
- name: Enable mkhomedir
  command: "chroot {{chroot_target}} /bin/bash -c 'pam-auth-update --enable mkhomedir'"

#ulimit
- name: Deploy ulimit template
  template:
    src: templates/limits.d.cluster.conf.j2
    dest: "{{chroot_target}}/etc/security/limits.d/cluster.conf"

- name: Stop locate going into mounts
  copy:
    dest: "{{chroot_target}}/etc/updatedb.findutils.cron.local"
    content: |
      PRUNEPATHS="/tmp /usr/tmp /var/tmp /var/spool /sfs /media {{client_mounts | map(attribute='path') | join(' ')}}"

#Compat
- name: Make backwards compatible folder structure
  file:
    path: "{{chroot_target}}/cm"
    state: directory
- name: Make backwards compatible folder structure
  file:
    path: "{{chroot_target}}/cm/shared"
    src: "{{shared_path}}"
    state: link
    force: yes

# sudo
- name: Deploy sudoers configs (application specific)
  template:
    src : "{{ item }}"
    dest: "{{chroot_target}}/etc/sudoers.d/{{ item | basename | regex_replace('\\.j2$', '') }}"
    owner: root
    group: root
    mode: "0644"
  with_fileglob:
    - "templates/sudoers_*.j2"

# Python
- name: Install python dependency packages
  command: "chroot {{chroot_target}} /bin/bash -c 'apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -y install libffi-dev tk-dev libsqlite3-dev libbz2-dev liblzma-dev libgdbm-dev libxml2-dev libxslt1-dev libhdf5-dev'"

#Intel compiler
- name: Install Intel Compiler dependency packages
  command: "chroot {{chroot_target}} /bin/bash -c 'apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -o Dpkg::Options::=\"--force-confold\" -y install libgbm1 libgtk-3-0'"

# QEMU guest agent
- name: Install qemu guest agent
  command: "chroot {{chroot_target}} /bin/bash -c 'apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -y install qemu-guest-agent'"

#Custom user stuff
- name: Install extra user packages
  command: "chroot {{chroot_target}} /bin/bash -c 'apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -o Dpkg::Options::=\"--force-confold\" -y install {{extra_user_apps}}'"

# EESSI (follow https://www.eessi.io/docs/getting_access/native_installation/#__tabbed_1_2)
#
#- name: Install lsb-release for CernVM-FS
#  command: "chroot {{chroot_target}} /bin/bash -c 'apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -o Dpkg::Options::=\"--force-confold\" -y install lsb-release'"
#
#- name: Download cvmfs-release-latest_all
#  ansible.builtin.get_url:
#    url: https://ecsft.cern.ch/dist/cvmfs/cvmfs-release/cvmfs-release-latest_all.deb
#    dest: "{{chroot_target}}/tmp/cvmfs-release-latest_all.deb"
#
#- name: Install cvmfs-release-latest_all
#  command: "chroot {{chroot_target}} /bin/bash -c 'dpkg -i /tmp/cvmfs-release-latest_all.deb'"
#
#- name: Install CernVM-FS
#  command: "chroot {{chroot_target}} /bin/bash -c 'apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -o Dpkg::Options::=\"--force-confold\" -y install cvmfs'"
#
#- name: Download EESSI configuration for CernVM-FS
#  ansible.builtin.get_url:
#    url: https://github.com/EESSI/filesystem-layer/releases/download/latest/cvmfs-config-eessi_latest_all.deb
#    dest: "{{chroot_target}}/tmp/cvmfs-config-eessi_latest_all.deb"
#
#- name: Install EESSI configuration for CernVM-FS
#  command: "chroot {{chroot_target}} /bin/bash -c 'dpkg -i /tmp/cvmfs-config-eessi_latest_all.deb'"
#
#- name: Create client configuration file for CernVM-FS (no squid proxy, 10GB local CernVM-FS client cache)
#  lineinfile:
#    path: "{{chroot_target}}/etc/cvmfs/default.local"
#    line: "{{ item.line }}"
#    create: yes
#  loop:
#    - { line: 'CVMFS_CLIENT_PROFILE="single"' }
#    - { line: 'CVMFS_QUOTA_LIMIT=10000' }
#
#- name: Make sure that EESSI CernVM-FS repository is accessible
#  command: "chroot {{chroot_target}} /bin/bash -c 'cvmfs_config setup'"

- name: Include nexpose task list in play
  include_tasks:
    file: clients/nexpose.yaml

- name: Include clean up task list in play
  include_tasks:
    file: clients/cleanup.yaml
