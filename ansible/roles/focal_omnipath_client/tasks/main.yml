#Copyright (c) 2023 Wageningen University
#Author: Gwen Dawes <gwen.dawes@wur.nl>
#        Jan van Haarst <jan.vanhaarst@wur.nl>
#        Fons Prinsen <fons.prinsen@wur.nl>

- fail: msg="You must define chroot_target!"
  when: chroot_target is undefined

- name: install prerequisites
  apt:
    pkg:
      - debootstrap

- name: Check chroot presence
  stat:
    path: "{{chroot_target}}"
  register: chroot_stat

- name: debootstrap initial image
  command: "debootstrap focal {{chroot_target}}"
  when: chroot_stat.stat.isdir is not defined

- name: Add timestamp for version tracking
  command: "chroot {{chroot_target}} /bin/bash -c 'date +%s > /etc/sali_version'"

- name: Add base repos
  copy:
    dest: "{{chroot_target}}/etc/apt/sources.list"
    content: |
      deb http://nl.archive.ubuntu.com/ubuntu focal main restricted universe multiverse
      deb http://nl.archive.ubuntu.com/ubuntu focal-updates main restricted universe multiverse
      deb http://nl.archive.ubuntu.com/ubuntu focal-security main restricted universe multiverse

- name: Update base software
  command: "chroot {{chroot_target}} /bin/bash -c 'apt-get update && apt-get dist-upgrade --assume-yes'"

- name: Install extra base software
  command: "chroot {{chroot_target}} /bin/bash -c 'apt-get update && apt-get install -y nano netbase isc-dhcp-client systemd-sysv sudo initramfs-tools cloud-init netplan.io overlayroot rsync openssh-server ipmitool nfs-common libipmimonitoring6 freeipmi-tools jq curl libcurl4-openssl-dev'"

- name: Add a root password
  command: "chroot {{chroot_target}} /bin/bash -c 'echo 'root:{{node_root_hash}}' | chpasswd -e'"

- name: Make client ssh folder
  file:
    path: "{{chroot_target}}/root/.ssh"
    state: directory
    owner: root
    group: root
    mode: '0700'

- name: Make client ssh authorized_keys file
  file:
    path: "{{chroot_target}}/root/.ssh/authorized_keys"
    state: touch
    owner: root
    group: root
    mode: '0600'

- name: Copy ssh key to client
  copy:
      dest: "{{chroot_target}}/root/.ssh/authorized_keys"
      mode: '0600'
      content: "{{root_ssh_pubkey}}"

- block:
  - name: "Set ssh host private keys"
    copy:
      dest: "{{chroot_target}}/etc/ssh/ssh_host_{{item.type}}_key"
      mode: '0600'
      content: "{{item.key}}"
    with_items: "{{client_ssh_keys}}"

  - name: "Generate ssh host public keys"
    command: "chroot {{chroot_target}} /bin/bash -c 'ssh-keygen -y -f /etc/ssh/ssh_host_{{item.type}}_key > /etc/ssh/ssh_host_{{item.type}}_key.pub'"
    with_items: "{{client_ssh_keys}}"
  when: client_ssh_keys is defined

#efi config unnecessary here as it's set in the post_installer scripts in SALI
#Hostname gets updated by sali
- name: Set hostname
  copy:
    dest: "{{chroot_target}}/etc/hostname"
    content: focal-lustre

#Netplan config created by sali
#fstab created by sali

- name: Remove machine-id from /etc
  file: 
    path: "{{chroot_target}}/etc/machine-id"
    state: absent

- name: Remove machine-id from /var/lib/dbus
  file: 
    path: "{{chroot_target}}/var/lib/dbus/machine-id"
    state: absent

- name: Check resolv.conf state
  stat:
    path: "{{chroot_target}}/etc/resolv.conf"
  register: resolvconf_stat

- name: Remove resolv.conf symlink
  file:
    path: "{{chroot_target}}/etc/resolv.conf"
    state: absent
  when: resolvconf_stat.stat.islnk is defined and resolvconf_stat.stat.islnk

- name: Add resolv.conf contents
  copy:
    dest: "{{chroot_target}}/etc/resolv.conf"
    content: |
      nameserver {{shared_master['networks'][default_network]['ip'] | ipaddr('address')}}
      search {{networks[default_network]['zone']}}

- name: Install lustre build packages
  command: "chroot {{chroot_target}} /bin/bash -c 'apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -y install pkg-config perl lsof autoconf automake dpatch debhelper libltdl-dev bison libgfortran4 pciutils ethtool make swig gcc chrpath graphviz autotools-dev quilt flex dkms tk tcl libnuma1 gfortran libyaml-dev libnl-3-200 libnl-route-3-200 zlib1g-dev module-assistant libreadline-dev libselinux-dev libsnmp-dev libssl-dev gcc-8'"

- name: Check kernel module presence
  stat:
    path: "{{chroot_target}}/lib/modules/{{lustre_client_kernel_version}}-generic/"
  register: kernel_module_stat

- name: Get current lustre source package name
  shell: "curl --connect-timeout 5 --max-time 10 --retry 5 --retry-delay 0 --retry-max-time 40 https://downloads.whamcloud.com/public/lustre/lustre-{{lustre_client_version}}/ubuntu2204/client/Packages | grep Filename | grep lustre-source | cut -d '/' -f 2"
  args: {warn: false}
  register: lustre_source_shell

- set_fact:
    lustre_source_package: "{{lustre_source_shell.stdout}}"

- name: Lustre kernel installation
  block:
  - name: Get current master kernel
    command: "uname -r"
    register: uname_r_shell

  - name: Get current kernel package version
    shell: "curl --connect-timeout 5 --max-time 10 --retry 5 --retry-delay 0 --retry-max-time 40 {{lustre_client_kernel_pool}} | grep {{lustre_client_kernel_version}}-generic | grep amd | sed 's/<[^>]*>/ /g' | sed 's/  */ /g' | cut -d ' ' -f 2 | grep linux-headers | cut -d '-' -f 3-8"
    args: {warn: false}
    register: kernel_generic_version_shell

  - name: Get current kernel package version (all)
    shell: "curl --connect-timeout 5 --max-time 10 --retry 5 --retry-delay 0 --retry-max-time 40 {{lustre_client_kernel_pool}} | grep {{lustre_client_kernel_version}} | grep all | sed 's/<[^>]*>/ /g' | sed 's/  */ /g' | cut -d ' ' -f 2 | grep headers"
    args: {warn: false}
    register: linux_headers_all_shell

  - set_fact:
      master_kernel: "{{uname_r_shell.stdout}}"
      kernel_version_deb_suffix: "{{kernel_generic_version_shell.stdout}}"
      linux_headers_all_package: "{{linux_headers_all_shell.stdout}}"

  - name: Add kernel modules workaround directory
    file:
      path: "{{chroot_target}}/lib/modules/{{lustre_client_kernel_version}}-generic"
      state: directory

  - name: Remove existing kernel modules directory
    file:
      path: "{{chroot_target}}/lib/modules/{{master_kernel}}"
      state: absent

  - name: Add kernel modules workaround symlink
    file:
      path: "{{chroot_target}}/lib/modules/{{master_kernel}}"
      src: "{{lustre_client_kernel_version}}-generic"
      state: link
      force: yes

  - name: Install kernel dependency packages
    command: "chroot {{chroot_target}} /bin/bash -c 'apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -y install crda wireless-crda'"
  - name: Remove current headers
    command: "chroot {{chroot_target}} /bin/bash -c 'apt-get -y remove linux-headers*'"

  - name: Download linux headers (specific)
    command: "wget -q {{lustre_client_kernel_pool}}/linux-headers-{{kernel_version_deb_suffix}} -O {{chroot_target}}/opt/linux-headers-{{kernel_version_deb_suffix}}"
    args: {warn: false}
  - name: Download linux image (unsigned)
    command: "wget -q {{lustre_client_kernel_pool}}/linux-image-unsigned-{{kernel_version_deb_suffix}} -O {{chroot_target}}/opt/linux-image-unsigned-{{kernel_version_deb_suffix}}"
    args: {warn: false}
  - name: Download linux modules
    command: "wget -q {{lustre_client_kernel_pool}}/linux-modules-{{kernel_version_deb_suffix}} -O {{chroot_target}}/opt/linux-modules-{{kernel_version_deb_suffix}}"
    args: {warn: false}
  - name: Download linux extra modules
    command: "wget -q {{lustre_client_kernel_pool}}/linux-modules-extra-{{kernel_version_deb_suffix}} -O {{chroot_target}}/opt/linux-modules-extra-{{kernel_version_deb_suffix}}"
    args: {warn: false}
  - name: Download linux headers (generic)
    command: "wget -q {{lustre_client_kernel_pool}}/{{linux_headers_all_package}} -O {{chroot_target}}/opt/{{linux_headers_all_package}}"
    args: {warn: false}
  - name: Install all kernel deb files
    command: "chroot {{chroot_target}} /bin/bash -c 'dpkg -i /opt/*.deb'"
  when: kernel_module_stat.stat.isdir is not defined

- name: Install grub-efi
  command: "chroot {{chroot_target}} /bin/bash -c 'apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -y install grub-efi grub-efi-amd64'"

- name: Install omnipath packages
  command: "chroot {{chroot_target}} /bin/bash -c 'apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -y install linux-firmware opa-basic-tools opa-fastfabric opa-fm libpsm2-dev rdma-core libpsm2-2 ibverbs-providers libfabric1'"

- name: Change name of omnipath adapters to use traditional naming
  replace:
    path: "{{chroot_target}}/lib/udev/rules.d/60-rdma-persistent-naming.rules"
    regexp: 'NAME_FALLBACK'
    replace: 'NAME_KERNEL'

- name: Install final lustre package dependencies
# dpkg fails on linux-headers-generic, so we install it. But it's the wrong version, so we remove it again after building lustre (it's a hack, sorry)
  command: "chroot {{chroot_target}} /bin/bash -c 'apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -y install attr libpython3-stdlib libpython3.8-minimal libpython3.8-stdlib python3-dev python-is-python3 libsgutils2-2 libtool-bin mpi-default-bin sg3-utils mpi-default-dev ed libkeyutils1 libkeyutils-dev libaio1 libmount-dev libnl-genl-3-dev git libkrb5-dev linux-headers-generic quota selinux-utils'"

- name: Check lustre kernel module presence
  stat:
    path: "{{chroot_target}}/lib/modules/{{lustre_client_kernel_version}}-generic/updates/net/ko2iblnd.ko"
  register: lustre_module_stat

- name: Lustre installation from source
  block:
  - name: Download lustre source
    command: "wget -q https://downloads.whamcloud.com/public/lustre/lustre-{{lustre_client_version}}/ubuntu2204/client/{{lustre_source_package}} -O {{chroot_target}}/opt/lustre-source.deb"
    args: {warn: false}
  - name: Unpack downloaded deb
    command: "chroot {{chroot_target}} /bin/bash -c 'dpkg-deb -R /opt/lustre-source.deb /opt/lustre-source-deb'"
  - name: Untar source package
    command: "chroot {{chroot_target}} /bin/bash -c 'tar -xf /opt/lustre-source-deb/usr/src/lustre*.tar.bz2 -C /opt'"
  - name: Generate configure scripts (using autoconf)
    command: "chroot {{chroot_target}} /bin/bash -c 'cd /opt/modules/lustre/ && sh autogen.sh'"
  - name: Configure Lustre packages
    command: "chroot {{chroot_target}} /bin/bash -c 'cd /opt/modules/lustre/ && CC=gcc-8 ./configure --with-o2ib --disable-server --with-linux=/lib/modules/{{lustre_client_kernel_version}}-generic/build --with-kernel-source-header=/usr/src/linux-headers-{{lustre_client_kernel_version}}-generic/'"
  - name: Make Lustre packages
    command: "chroot {{chroot_target}} /bin/bash -c 'cd /opt/modules/lustre/ && CC=gcc-8 make debs -j $(( $(nproc) / 2))'"
  - name: Install packages
    command: "chroot {{chroot_target}} /bin/bash -c 'dpkg -i /opt/modules/lustre/debs/lustre*.deb'"
  - name: Remove lustre-source (to allow rebuilding packages without failures)
    command: "chroot {{chroot_target}} /bin/bash -c 'apt-get -y remove lustre-source'"
  - name: Remove kernel modules workaround symlink
    file:
      path: "{{chroot_target}}/lib/modules/{{master_kernel}}"
      state: absent
  when: lustre_module_stat.stat.isreg is not defined

- name: Remove non-5.15 linux-headers-generic
  command: "chroot {{chroot_target}} /bin/bash -c 'apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -y remove linux-headers-generic'"

#lnet.conf handled by sali

- name: Make lnet start on boot
  file:
    path: "{{chroot_target}}/etc/systemd/system/multi-user.target.wants/lnet.service"
    src: "/lib/systemd/system/lnet.service"
    state: link
    force: yes

# This is required after the hwe kernel with lustre 2.15.5. It's a hack, we shouldn't need to run this 'manually' like this. Figure out how this actually works and make it pretty
- name: Run depmod before modprobing lnet kernel module
  lineinfile:
    path: "{{chroot_target}}/lib/systemd/system/lnet.service"
    insertafter: '^RemainAfterExit'
    line: ExecStartPre=/usr/sbin/depmod

#User auth configs
- name: Install auth config tools (1)
  command: "chroot {{chroot_target}} /bin/bash -c 'apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -y install sssd sssd-tools samba-common'"
- name: Install auth config tools (2)
  command: "chroot {{chroot_target}} /bin/bash -c 'apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -y install nslcd libnss-ldapd'"

# sssd
- name: Set up sssd config
  template:
    src: templates/client_sssd.conf.j2
    dest: "{{chroot_target}}/etc/sssd/sssd.conf"
    owner: root
    group: root
    mode: '0600'

- name: Set up nslcd config
  template:
    src: templates/client_nslcd.conf.j2
    dest: "{{chroot_target}}/etc/nslcd.conf"
    owner: root
    group: nslcd
    mode: '0640'
- name: Make ldap ssl cert folder
  file:
    path: "{{chroot_target}}/etc/ldap/ssl/"
    state: directory

- name: "Place ldap certificates"
  copy:
    dest: "{{chroot_target}}/etc/ldap/ssl/{{item.name}}"
    mode: '0600'
    content: "{{item.cert}}"
  with_items: "{{ldap_certs}}"

- name: Set up nsswitch
  template:
    src: templates/nsswitch.conf.j2
    dest: "{{chroot_target}}/etc/nsswitch.conf"
    owner: root
    group: root
    mode: '0644'

# Disable nscd service
- name: Remove nscd startup (stops sssd complaining)
  command: "find {{chroot_target}}/etc/ -name 'S01nscd' -exec rm {} \\;"
- name: Remove nscd startup from systemd too
  file: 
    path: "{{chroot_target}}/etc/systemd/system/multi-user.target.wants/nscd.service"
    state: absent
- name: mask nscd.service
  file:
    path: "{{chroot_target}}/etc/systemd/system/nscd.service"
    src: "/dev/null"
    state: link
    force: yes
# /Disable nscd service

- name: Make nslcd override dir
  file:
    path: "{{chroot_target}}/etc/systemd/system/nslcd.service.d"
    state: directory

- name: Make nslcd always restart
  copy:
    dest: "{{chroot_target}}/etc/systemd/system/nslcd.service.d/override.conf"
    content: |
      [Service]
      Restart=always

- name: Set up smb.conf
  template:
    src: templates/smb.conf.j2
    dest: "{{chroot_target}}/etc/samba/smb.conf"
    owner: root
    group: root
    mode: '0644'

#Copy filesystem mount files in
- name: Set up mount configs
  template:
    src: templates/client_mount.j2
    dest: "{{chroot_target}}/etc/systemd/system{{item.path}}.mount"
    owner: root
    group: root
    mode: '0644'
  with_items: "{{client_mounts}}"

- name: Set up automount configs
  template:
    src: templates/client_automount.j2
    dest: "{{chroot_target}}/etc/systemd/system{{item.path}}.automount"
    owner: root
    group: root
    mode: '0644'
  with_items: "{{client_mounts}}"

- name: Make mounts automount on boot
  file:
    path: "{{chroot_target}}/etc/systemd/system/multi-user.target.wants{{item.path}}.automount"
    src: "/etc/systemd/system{{item.path}}.automount"
    state: link
    force: yes
  with_items: "{{client_mounts}}"

#Create symlink for /scratch to /lustre/scratch
- name: Create symlink for /scratch
  file:
    path: "{{chroot_target}}/scratch"
    src: "/lustre/scratch"
    state: link
    force: yes

- name: Include slurmd task list in play
  include_tasks:
    file: clients/slurmd.yaml

#PAM
- name: Link pam_slurm_adopt into pam path
  file:
    path: "{{chroot_target}}/usr/lib/x86_64-linux-gnu/security/pam_slurm_adopt.so"
    src: "/shared/apps/slurm/current/lib/security/pam_slurm_adopt.so"
    state: link
    force: yes

- name: Set pam_slurm_adopt on
  lineinfile:
    path: "{{chroot_target}}/etc/pam.d/sshd"
    insertafter: "@include common-account"
    line: "-account    required      pam_slurm_adopt.so action_adopt_failure=deny action_generic_failure=deny"

#Overlayroot
- name: Set overlayroot on
  lineinfile:
    path: "{{chroot_target}}/etc/overlayroot.conf"
    regexp: '^overlayroot=.*'
    line: overlayroot="tmpfs:swap=1,recurse=0"

- name: Disable quiet grub
  lineinfile:
    path: "{{chroot_target}}/etc/default/grub"
    regexp: '.*GRUB_CMDLINE_LINUX_DEFAULT=.*'
    line: '#GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"'

- name: Enable swap accounting in grub
  lineinfile:
    path: "{{chroot_target}}/etc/default/grub"
    regexp: '.*GRUB_CMDLINE_LINUX=.*'
    line: 'GRUB_CMDLINE_LINUX="swapaccount=1"'

- name: Enable log forwarding
  lineinfile:
    path: "{{chroot_target}}/etc/rsyslog.conf"
    line: "*.* @{{shared_master['networks'][default_network]['fqdn']}}:514"

- name: Disable apt unattended upgrades
  replace:
      path: "{{chroot_target}}/etc/apt/apt.conf.d/20auto-upgrades"
      regexp: '1'
      replace: '0'

#Postfix
- name: Install system postfix
  command: "chroot {{chroot_target}} /bin/bash -c 'apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -y install postfix mailutils postfix-pcre'"

- name: Set up main postfix config
  template:
    src: "templates/postfix_main.cf.j2"
    dest: "{{chroot_target}}/etc/postfix/main.cf"
    owner: root
    group: root
    mode: "0644"

- name: Set up generic mapping config
  template:
    src: "templates/postfix_generic.pcre.j2"
    dest: "{{chroot_target}}/etc/postfix/generic.pcre"
    mode: "0644"

- name: Set up sender_canonical_maps
  template:
    src: "templates/postfix_sender_canonical_maps.j2"
    dest: "{{chroot_target}}/etc/postfix/sender_canonical_maps"
    mode: "0644"

- name: Set up header_checks
  template:
    src: "templates/postfix_header_checks.j2"
    dest: "{{chroot_target}}/etc/postfix/header_checks"
    mode: "0644"

- name: Set up smtp_header_checks
  template:
    src: "templates/postfix_smtp_header_checks.j2"
    dest: "{{chroot_target}}/etc/postfix/smtp_header_checks"
    mode: "0644"

- name: Set up sasl_passwd
  template:
    src: "templates/postfix_sasl_passwd.j2"
    dest: "{{chroot_target}}/etc/postfix/sasl/sasl_passwd"
    mode: "0600"

- name: Configure remapping database
  command: "postmap {{chroot_target}}/etc/postfix/generic.pcre"

- name: postmap header_checks
  command: "postmap {{chroot_target}}/etc/postfix/header_checks"

- name: postmap smtp_header_checks
  command: "postmap {{chroot_target}}/etc/postfix/smtp_header_checks"

- name: postmap sender_canonical_maps
  command: "postmap {{chroot_target}}/etc/postfix/sender_canonical_maps"

- name: postmap sasl_password
  command: "postmap {{chroot_target}}/etc/postfix/sasl/sasl_passwd"

- name: Set up mailutils config
  template:
    src: "templates/mailutils.conf.j2"
    dest: "{{chroot_target}}/etc/mailutils.conf"
    mode: "0644"

- name: Disable local logging of mail
  lineinfile:
    path: "{{chroot_target}}//etc/rsyslog.d/50-default.conf"
    regexp: ".*mail."
    state: absent

#Telegraf
- name: Mask telegraf service (if present)
  file:
      path: "{{chroot_target}}/etc/systemd/system/multi-user.target.wants/telegraf.service"
      src: "/dev/null"
      state: link
      force: yes

#Lmod
- name: Install lmod dependencies
  command: "chroot {{chroot_target}} /bin/bash -c 'apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -y install liblua5.3-0 liblua5.3-dev lua5.3 lua-posix luarocks tcl tcl-dev'"

- name: HACK - symlink the LMOD path to match SL7
  file:
    src: "{{apps_path}}/lmod"
    dest: "{{chroot_target}}/usr/share/lmod"
    force: yes
    state: link

- name: Add Lmod line to bashrc
  blockinfile:
    path: "{{chroot_target}}/etc/bash.bashrc"
    block: |
      #Set a default manpath
      export MANPATH=/usr/share/man
      # Enable lmod
      . {{apps_path}}/lmod/current/init/bash
      #HACK - this allows for backwards compatibility underlay with SL7
      export LMOD_CMD=/usr/share/lmod/lmod/libexec/lmod
      #Set the modulepath
      export MODULEPATH={{modules_path}}

- name: Remove Ubuntu motd
  replace:
      path: "{{chroot_target}}/etc/pam.d/{{item}}"
      regexp: '^(session.*optional.*pam_motd.so)'
      replace: '#\1'
  with_items:
  - sshd
  - login

#ntp
- name: Include ntp task list in play
  include_tasks:
    file: clients/ntp.yaml

#ulimit
- name: Deploy ulimit template
  template:
    src: templates/limits.d.cluster.conf.j2
    dest: "{{chroot_target}}/etc/security/limits.d/cluster.conf"

- name: Stop locate going into mounts
  copy:
    dest: "{{chroot_target}}/etc/updatedb.findutils.cron.local"
    content: |
      PRUNEPATHS="/tmp /usr/tmp /var/tmp /var/spool /sfs /media {{client_mounts | map(attribute='path') | join(' ')}}"

#Compat
- name: Make backwards compatible folder structure
  file:
    path: "{{chroot_target}}/cm"
    state: directory
- name: Make backwards compatible folder structure
  file:
    path: "{{chroot_target}}/cm/shared"
    src: "{{shared_path}}"
    state: link
    force: yes

# Python
- name: Install python dependency packages
  command: "chroot {{chroot_target}} /bin/bash -c 'apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -y install libffi-dev tk-dev libsqlite3-dev libbz2-dev liblzma-dev libgdbm-dev libxml2-dev libxslt1-dev libhdf5-dev'"

#Intel compiler
- name: Install Intel Compiler dependency packages
  command: "chroot {{chroot_target}} /bin/bash -c 'apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -o Dpkg::Options::=\"--force-confold\" -y install libgbm1 libgtk-3-0'"

#Custom user stuff
- name: Install extra user packages
  command: "chroot {{chroot_target}} /bin/bash -c 'apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -o Dpkg::Options::=\"--force-confold\" -y install {{extra_user_apps}} {{ood_apps}}'"

- name: Install extra xfce4 packages
  command: "chroot {{chroot_target}} /bin/bash -c 'apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -o Dpkg::Options::=\"--force-confold\" -y install --no-install-recommends {{xfce_apps}}'"

# EESSI (follow https://www.eessi.io/docs/getting_access/native_installation/#__tabbed_1_2)

#- name: Install lsb-release for CernVM-FS
#  command: "chroot {{chroot_target}} /bin/bash -c 'apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -o Dpkg::Options::=\"--force-confold\" -y install lsb-release'"
#
#- name: Download cvmfs-release-latest_all
#  ansible.builtin.get_url:
#    url: https://ecsft.cern.ch/dist/cvmfs/cvmfs-release/cvmfs-release-latest_all.deb
#    dest: "{{chroot_target}}/tmp/cvmfs-release-latest_all.deb"
#
#- name: Install cvmfs-release-latest_all
#  command: "chroot {{chroot_target}} /bin/bash -c 'dpkg -i /tmp/cvmfs-release-latest_all.deb'"
#
#- name: Install CernVM-FS
#  command: "chroot {{chroot_target}} /bin/bash -c 'apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -o Dpkg::Options::=\"--force-confold\" -y install cvmfs'"
#
#- name: Download EESSI configuration for CernVM-FS
#  ansible.builtin.get_url:
#    url: https://github.com/EESSI/filesystem-layer/releases/download/latest/cvmfs-config-eessi_latest_all.deb
#    dest: "{{chroot_target}}/tmp/cvmfs-config-eessi_latest_all.deb"
#
#- name: Install EESSI configuration for CernVM-FS
#  command: "chroot {{chroot_target}} /bin/bash -c 'dpkg -i /tmp/cvmfs-config-eessi_latest_all.deb'"
#
#- name: Create client configuration file for CernVM-FS (no squid proxy, 10GB local CernVM-FS client cache)
#  lineinfile:
#    path: "{{chroot_target}}/etc/cvmfs/default.local"
#    line: "{{ item.line }}"
#    create: yes
#  loop:
#    - { line: 'CVMFS_CLIENT_PROFILE="single"' }
#    - { line: 'CVMFS_QUOTA_LIMIT=10000' }
#
#- name: Make sure that EESSI CernVM-FS repository is accessible
#  command: "chroot {{chroot_target}} /bin/bash -c 'cvmfs_config setup'"

- name: Include clean up task list in play
  include_tasks:
    file: clients/cleanup.yaml
