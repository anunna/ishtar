This role is used for installing a keycloak OIDC/OAUTH/SAML2 provider so that all web services
can use a unified mode of authentication.

!!KNOWN BUGS!!

In at least the 19.0.2 release, the admin page (accessible from localhost only) doesn't work in Firefox.

To access it, you need to run the entire service in dev mode via './bin/kc.sh start-dev'. This sucks.
Thankfully, the apache config shields a lot of bad actors away from the service, so it should be
capable of running like this for a while.
