#!/bin/bash
DC=$(ldapsearch -Y EXTERNAL -Q -H ldapi:/// -s base -b "" + | grep namingContexts | cut -d ' ' -f 2-99)

while getopts ":g:G:" options; do
  case "${options}" in
    g)
      LDAP_GROUPNAME=${OPTARG}
      ;;
    G)
      LDAP_GID=${OPTARG}
      ;;
    :)
      echo "Error: -${OPTARG} requires an argument."
      ;;
    *)
      ;;
  esac
done

if [ "x$LDAP_GROUPNAME" == "x" ] ; then
  read -p "Please tell me the group name: " LDAP_GROUPNAME
fi
if [ "x$LDAP_GID" == "x" ] ; then
  read -p "Please tell me the user gid: " LDAP_GID
fi

ldapmodify -Y EXTERNAL -H ldapi:/// << EOF
dn: cn=$LDAP_GROUPNAME,ou=groups,$DC
changetype: add
objectClass: posixGroup
cn: $LDAP_GROUPNAME
description: $LDAP_GROUPNAME
gidNumber: $LDAP_GID
EOF

RET=$?
if [ $RET -eq 68 ] ; then #entryAlreadyExists
  exit 0
fi

exit $RET
