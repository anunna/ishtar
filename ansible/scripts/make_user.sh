#!/bin/bash
DC=$(ldapsearch -Y EXTERNAL -Q -H ldapi:/// -s base -b "" + | grep namingContexts | cut -d ' ' -f 2-99)

while getopts ":u:U:G:p:h:s:" options; do
  case "${options}" in
    u)
      LDAP_USERNAME=${OPTARG}
      ;;
    U)
      LDAP_UID=${OPTARG}
      ;;
    G)
      LDAP_GID=${OPTARG}
      ;;
    p)
      LDAP_PASSWORD=${OPTARG}
      ;;
    h)
      LDAP_HOMEDIR=${OPTARG}
      ;;
    s)
      LDAP_SHELL=${OPTARG}
      ;;
    :)
      echo "Error: -${OPTARG} requires an argument."
      ;;
    *)
      ;;
  esac
done

if [ "x$LDAP_USERNAME" == "x" ] ; then
  read -p "Please tell me the user name: " LDAP_USERNAME
fi
if [ "x$LDAP_UID" == "x" ] ; then
  read -p "Please tell me the user uid: " LDAP_UID
fi
if [ "x$LDAP_GID" == "x" ] ; then
  read -p "Please tell me the user gid: " LDAP_GID
fi
if [ "x$LDAP_PASSWORD" == "x" ] ; then
  read -sp "Please tell me the user password: " LDAP_PASSWORD
fi
if [ "x$LDAP_HOMEDIR" == "x" ] ; then
  LDAP_HOMEDIR=/home/$LDAP_USERNAME
fi
if [ "x$LDAP_SHELL" == "x" ] ; then
  LDAP_HOMEDIR=/bin/bash
fi

ldapmodify -Y EXTERNAL -H ldapi:/// << EOF
dn: cn=$LDAP_USERNAME,ou=users,$DC
changetype: add
objectClass: inetOrgPerson
objectClass: posixAccount
objectClass: shadowAccount
sn: $LDAP_USERNAME
cn: $LDAP_USERNAME
description: $LDAP_USERNAME
uid: $LDAP_USERNAME
uidNumber: $LDAP_UID
gidNumber: $LDAP_GID
homeDirectory: $LDAP_HOMEDIR
userPassword: $LDAP_PASSWORD
loginShell: $LDAP_SHELL
EOF

RET=$?
if [ $RET -eq 68 ] ; then #entryAlreadyExists
  exit 0
fi

exit $RET
