#!/bin/bash
while getopts ":K:P:r:n:o:b:R:" options; do
  case "${options}" in
    K)
      KC_USER=${OPTARG}
      ;;
    P)
      KC_PASS=${OPTARG}
      ;;
    r)
      REALM=${OPTARG}
      ;;
    n)
      CLIENT_NAME=${OPTARG}
      ;;
    o)
      ROOT_URL=${OPTARG}
      ;;
    b)
      BASE_URL=${OPTARG}
      ;;
    R)
      REDIRECT_URL=${OPTARG}
      ;;
    :)
      echo "Error: -${OPTARG} requires an argument."
      ;;
    *)
      ;;
  esac
done

if [ "x$KC_USER" == "x" ] ; then
  read -p "Please tell me the keycloak admin user: " KC_USER
fi
if [ "x$KC_PASS" == "x" ] ; then
  read -sp "Please tell me the keycloak admin password: " KC_PASS
fi
if [ "x$REALM" == "x" ] ; then
  read -p "Please tell me the realm: " REALM
fi
if [ "x$CLIENT_NAME" == "x" ] ; then
  read -p "Please tell me the client name: " CLIENT_NAME
fi
if [ "x$ROOT_URL" == "x" ] ; then
  read -p "Please tell me the root URL: " ROOT_URL
fi
if [ "x$BASE_URL" == "x" ] ; then
  BASE_URL="$ROOT_URL"
fi
if [ "x$REDIRECT_URL" == "x" ] ; then
  REDIRECT_URL="$ROOT_URL/*"
fi

su - keycloak -s /bin/bash -c "./bin/kcadm.sh config credentials --server http://localhost:8564 --realm master --user $KC_USER --password $KC_PASS"

su - keycloak -s /bin/bash -c "./bin/kcadm.sh create -r $REALM clients -s name=$CLIENT_NAME -s clientId=$CLIENT_NAME -s rootUrl=$ROOT_URL -s baseUrl=$BASE_URL -s redirectUris+=$REDIRECT_URL"

su - keycloak -s /bin/bash -c "./bin/kcadm.sh get -r $REALM clients  | jq -r '.[] | select(.name==\"$CLIENT_NAME\") | .secret'"
