#!/bin/bash
DC=$(ldapsearch -Y EXTERNAL -Q -H ldapi:/// -s base -b "" + | grep namingContexts | cut -d ' ' -f 2-99)

while getopts ":u:p:" options; do
  case "${options}" in
    u)
      LDAP_USERNAME=${OPTARG}
      ;;
    p)
      LDAP_PASSWORD=${OPTARG}
      ;;
    :)
      echo "Error: -${OPTARG} requires an argument."
      ;;
    *)
      ;;
  esac
done

if [ "x$LDAP_USERNAME" == "x" ] ; then
  read -p "Please tell me the user name: " LDAP_USERNAME
fi
if [ "x$LDAP_PASSWORD" == "x" ] ; then
  read -sp "Please tell me the user password: " LDAP_PASSWORD
fi

#dn: cn=$LDAP_USERNAME,$DC
ldapmodify -Y EXTERNAL -H ldapi:/// << EOF
dn: cn=$LDAP_USERNAME,ou=users,$DC
changetype: modify
replace: userPassword
userPassword: $LDAP_PASSWORD
EOF
