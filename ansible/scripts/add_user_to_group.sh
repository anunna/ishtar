#!/bin/bash
DC=$(ldapsearch -Y EXTERNAL -Q -H ldapi:/// -s base -b "" + | grep namingContexts | cut -d ' ' -f 2-99)

while getopts ":u:g:" options; do
  case "${options}" in
    u)
      LDAP_USERNAME=${OPTARG}
      ;;
    g)
      LDAP_GROUPNAME=${OPTARG}
      ;;
    :)
      echo "Error: -${OPTARG} requires an argument."
      ;;
    *)
      ;;
  esac
done

if [ "x$LDAP_USERNAME" == "x" ] ; then
  read -p "Please tell me the user name: " LDAP_USERNAME
fi
if [ "x$LDAP_GROUPNAME" == "x" ] ; then
  read -p "Please tell me the group name: " LDAP_GROUPNAME
fi

ldapmodify -Y EXTERNAL -H ldapi:/// << EOF
dn: cn=$LDAP_GROUPNAME,ou=groups,$DC
changetype: modify
add: memberUid
memberUid: $LDAP_USERNANE
EOF

RET=$?
if [ $RET -eq 20 ] ; then #attributeOrValueExists
  exit 0
fi

exit $RET
