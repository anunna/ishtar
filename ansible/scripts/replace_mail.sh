#!/bin/bash
DC=$(ldapsearch -Y EXTERNAL -Q -H ldapi:/// -s base -b "" + | grep namingContexts | cut -d ' ' -f 2-99)

while getopts ":u:e:" options; do
  case "${options}" in
    u)
      LDAP_USERNAME=${OPTARG}
      ;;
    e)
      LDAP_EMAIL=${OPTARG}
      ;;
    :)
      echo "Error: -${OPTARG} requires an argument."
      ;;
    *)
      ;;
  esac
done

if [ "x$LDAP_USERNAME" == "x" ] ; then
  read -p "Please tell me the user name: " LDAP_USERNAME
fi
if [ "x$LDAP_EMAIL" == "x" ] ; then
  read -p "Please tell me the user email: " LDAP_EMAIL
fi

ldapmodify -Y EXTERNAL -H ldapi:/// << EOF
dn: cn=$LDAP_USERNAME,ou=users,$DC
changetype: modify
replace: mail
mail: $LDAP_EMAIL
EOF
