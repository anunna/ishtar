#!/bin/bash
while getopts ":K:P:r:u:" options; do
  case "${options}" in
    K)
      KC_USER=${OPTARG}
      ;;
    P)
      KC_PASS=${OPTARG}
      ;;
    r)
      KC_REALM=${OPTARG}
      ;;
    u)
      KC_USERNAME=${OPTARG}
      ;;
    :)
      echo "Error: -${OPTARG} requires an argument."
      ;;
    *)
      ;;
  esac
done

if [ "x$KC_USER" == "x" ] ; then
  read -p "Please tell me the keycloak admin user: " KC_USER
fi
if [ "x$KC_PASS" == "x" ] ; then
  read -sp "Please tell me the keycloak admin password: " KC_PASS
fi
if [ "xKC_$REALM" == "x" ] ; then
  read -p "Please tell me the realm: " KC_REALM
fi
if [ "x$KC_USERNAME" == "x" ] ; then
  read -p "Please tell me the keycloak user of interest: " KC_USERNAME
fi

su - keycloak -s /bin/bash -c "./bin/kcadm.sh config credentials --server http://localhost:8564 --realm master --user $KC_USER --password $KC_PASS"

su - keycloak -s /bin/bash -c "./bin/kcadm.sh get -r $KC_REALM users -q username=$KC_USERNAME"
su - keycloak -s /bin/bash -c "./bin/kcadm.sh get-roles -r $KC_REALM --uusername=$KC_USERNAME"
