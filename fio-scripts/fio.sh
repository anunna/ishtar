#!/bin/bash
ssh node5 /bin/bash -c "/shared/apps/fio/3.29/bin/fio --server" &
ssh node6 /bin/bash -c "/shared/apps/fio/3.29/bin/fio --server" &

/shared/apps/fio/3.29/bin/fio --client <(echo -e 'node5') lustre-512-randread.fio --output-format=json | tee output-lustre-1n-randread-512-$(date +%s).json
/shared/apps/fio/3.29/bin/fio --client <(echo -e 'node5') lustre-1024-randread.fio --output-format=json | tee output-lustre-1n-randread-1024-$(date +%s).json
/shared/apps/fio/3.29/bin/fio --client <(echo -e 'node5') lustre-2048-randread.fio --output-format=json | tee output-lustre-1n-randread-2048-$(date +%s).json
/shared/apps/fio/3.29/bin/fio --client <(echo -e 'node5') lustre-4096-randread.fio --output-format=json | tee output-lustre-1n-randread-4096-$(date +%s).json
/shared/apps/fio/3.29/bin/fio --client <(echo -e 'node5') lustre-512-randwrite.fio --output-format=json | tee output-lustre-1n-randwrite-512-$(date +%s).json
/shared/apps/fio/3.29/bin/fio --client <(echo -e 'node5') lustre-1024-randwrite.fio --output-format=json | tee output-lustre-1n-randwrite-1024-$(date +%s).json
/shared/apps/fio/3.29/bin/fio --client <(echo -e 'node5') lustre-2048-randwrite.fio --output-format=json | tee output-lustre-1n-randwrite-2048-$(date +%s).json
/shared/apps/fio/3.29/bin/fio --client <(echo -e 'node5') lustre-4096-randwrite.fio --output-format=json | tee output-lustre-1n-randwrite-4096-$(date +%s).json

/shared/apps/fio/3.29/bin/fio --client <(echo -e 'node5\nnode6') lustre-512-randread.fio --output-format=json | tee output-lustre-2n-randread-512-$(date +%s).json
/shared/apps/fio/3.29/bin/fio --client <(echo -e 'node5\nnode6') lustre-1024-randread.fio --output-format=json | tee output-lustre-2n-randread-1024-$(date +%s).json
/shared/apps/fio/3.29/bin/fio --client <(echo -e 'node5\nnode6') lustre-2048-randread.fio --output-format=json | tee output-lustre-2n-randread-2048-$(date +%s).json
/shared/apps/fio/3.29/bin/fio --client <(echo -e 'node5\nnode6') lustre-4096-randread.fio --output-format=json | tee output-lustre-2n-randread-4096-$(date +%s).json
/shared/apps/fio/3.29/bin/fio --client <(echo -e 'node5\nnode6') lustre-512-randwrite.fio --output-format=json | tee output-lustre-2n-randwrite-512-$(date +%s).json
/shared/apps/fio/3.29/bin/fio --client <(echo -e 'node5\nnode6') lustre-1024-randwrite.fio --output-format=json | tee output-lustre-2n-randwrite-1024-$(date +%s).json
/shared/apps/fio/3.29/bin/fio --client <(echo -e 'node5\nnode6') lustre-2048-randwrite.fio --output-format=json | tee output-lustre-2n-randwrite-2048-$(date +%s).json
/shared/apps/fio/3.29/bin/fio --client <(echo -e 'node5\nnode6') lustre-4096-randwrite.fio --output-format=json | tee output-lustre-2n-randwrite-4096-$(date +%s).json

/shared/apps/fio/3.29/bin/fio --client <(echo -e 'node5') memtest-512-randread.fio --output-format=json | tee output-memtest-1n-randread-512-$(date +%s).json
/shared/apps/fio/3.29/bin/fio --client <(echo -e 'node5') memtest-1024-randread.fio --output-format=json | tee output-memtest-1n-randread-1024-$(date +%s).json
/shared/apps/fio/3.29/bin/fio --client <(echo -e 'node5') memtest-2048-randread.fio --output-format=json | tee output-memtest-1n-randread-2048-$(date +%s).json
/shared/apps/fio/3.29/bin/fio --client <(echo -e 'node5') memtest-4096-randread.fio --output-format=json | tee output-memtest-1n-randread-4096-$(date +%s).json
/shared/apps/fio/3.29/bin/fio --client <(echo -e 'node5') memtest-512-randwrite.fio --output-format=json | tee output-memtest-1n-randwrite-512-$(date +%s).json
/shared/apps/fio/3.29/bin/fio --client <(echo -e 'node5') memtest-1024-randwrite.fio --output-format=json | tee output-memtest-1n-randwrite-1024-$(date +%s).json
/shared/apps/fio/3.29/bin/fio --client <(echo -e 'node5') memtest-2048-randwrite.fio --output-format=json | tee output-memtest-1n-randwrite-2048-$(date +%s).json
/shared/apps/fio/3.29/bin/fio --client <(echo -e 'node5') memtest-4096-randwrite.fio --output-format=json | tee output-memtest-1n-randwrite-4096-$(date +%s).json

/shared/apps/fio/3.29/bin/fio --client <(echo -e 'node5\nnode6') memtest-512-randread.fio --output-format=json | tee output-memtest-2n-randread-512-$(date +%s).json
/shared/apps/fio/3.29/bin/fio --client <(echo -e 'node5\nnode6') memtest-1024-randread.fio --output-format=json | tee output-memtest-2n-randread-1024-$(date +%s).json
/shared/apps/fio/3.29/bin/fio --client <(echo -e 'node5\nnode6') memtest-2048-randread.fio --output-format=json | tee output-memtest-2n-randread-2048-$(date +%s).json
/shared/apps/fio/3.29/bin/fio --client <(echo -e 'node5\nnode6') memtest-4096-randread.fio --output-format=json | tee output-memtest-2n-randread-4096-$(date +%s).json
/shared/apps/fio/3.29/bin/fio --client <(echo -e 'node5\nnode6') memtest-512-randwrite.fio --output-format=json | tee output-memtest-2n-randwrite-512-$(date +%s).json
/shared/apps/fio/3.29/bin/fio --client <(echo -e 'node5\nnode6') memtest-1024-randwrite.fio --output-format=json | tee output-memtest-2n-randwrite-1024-$(date +%s).json
/shared/apps/fio/3.29/bin/fio --client <(echo -e 'node5\nnode6') memtest-2048-randwrite.fio --output-format=json | tee output-memtest-2n-randwrite-2048-$(date +%s).json
/shared/apps/fio/3.29/bin/fio --client <(echo -e 'node5\nnode6') memtest-4096-randwrite.fio --output-format=json | tee output-memtest-2n-randwrite-4096-$(date +%s).json
