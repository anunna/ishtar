#!/usr/bin/env python3
import sys, os
import json
raw = json.load(sys.stdin)

#read
readdata = {}
writedata = {}
for x in raw['client_stats']:
    if 'job options' in x:
        if 'rw' in x['job options']:
            if x['job options']['rw'] == 'randread':
                if x['jobname'] not in readdata: readdata[x['jobname']] = {}
                readdata[x['jobname']]['iops'] = x['read']['iops']
                readdata[x['jobname']]['bw'] = x['read']['bw']
            if x['job options']['rw'] == 'randwrite':
                if x['jobname'] not in readdata: writedata[x['jobname']] = {}
                writedata[x['jobname']]['iops'] = x['write']['iops']
                writedata[x['jobname']]['bw'] = x['write']['bw']

print(readdata)
print(writedata)
