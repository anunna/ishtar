# Ishtar
This repo contains tools and scripts for building an entire High Performance
Computing Cluster (HPC) from scratch. It is used extensively at Wageningen
University and Research (WUR) for the deployment of their main cluster, Anunna,
as well as other acceptance environments.

The repo is initially tailored to its primary use at WUR, but is quite open to
modifications for more generic clusters. It has been written with an intent to
be used by other environments, either as is, or as an inspiration for other
work.

The core of the work is based around the SURFsara project SALI, the SURFsara
Automated Linux Installer. This is used for the deployment of compute nodes.

The rest of the work details the construction of the basic infrastructure -
DNS, DHCP, TFTP, NFS, NTP, LDAP and the scheduler, Slurm.

## CAVEATS FOR INSTALLATION
Because this work is initially very situation specific, as of the moment of
writing, the following configurations are assumed:

* A High Availability setup (that is, two masters)
* A shared filesystem that is typically mounted between these nodes via e.g.
SAS, iSCSI

